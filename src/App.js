import './App.css';
import SportsTable from './components/SportsTable';

function App() {
  return (
    <div className="App">
      <SportsTable />
    </div>
  );
}

export default App;
